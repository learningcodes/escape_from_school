import java.util.*;
public class Player
{
    private int strength = 100;
    private Map<String, Thing> bag = new HashMap<>();
    private boolean specialItem =false;
    
    public void step() {
        strength -= 10;
        System.out.println("移动消耗10点体力，当前体力值为"+this.strength+"点");
        if (isDead()) {
            System.out.println("GAME  OVER!");
        }
    }
    
    public void pick(Thing thing) {
        bag.put(thing.getName(), thing);
    }
    
    public void eat(String name) {
       Thing thing = bag.get(name);
       if(thing != null) {
           strength += thing.getEnergy();
           if(strength >100) {
                strength = 100;
           }
       }
    }
    
    public void hurt(int damage) {
        strength -= damage;
        if (isDead()) {
            System.out.println("GAME  OVER!");
        }
    }
    
    public boolean isDead() {
        return strength <= 0; 
    }
    
    public void addToBag(Thing thing) {
        if (thing == null) {
            System.out.println("没东西可以捡!");
        }
        else {
            pick(thing);
            System.out.println( thing.getName() +"成功加入背包!");
            if(thing.getName()=="游戏机")
                this.specialItem=true;
        }
    }
    
    public void checkAndPrint() {
        if (bag.isEmpty()) {
            System.out.println("你的背包里面啥子都没得咯!");
        }
        else {
            System.out.println("你的背包有如下东西:");
            for (String key : bag.keySet()) {
                System.out.println("名称:   "+bag.get(key).getName() + "   回复能量" + bag.get(key).getEnergy() + "点");
            }
        }
    }
    
    public void eatThing(Command command) {
        if(!command.hasSecondWord()) {
            System.out.println("Eat what?");
            return;
        }
        
        String key = command.getSecondWord();
        if (bag.containsKey(key)) {
            eat(key);
            System.out.println("你的能量回复了" + bag.get(key).getEnergy()+"点!");
            System.out.println("你当前的能量是" + strength + "点!");
        }
        else {
            System.out.println("抱歉! 使用前请查看背包中是否存在该物件!");
            System.out.println("你可以通过'check'命令来查看背包中的物品!");
        }
        bag.remove(key);
    }
    public boolean getSpecialItem(){
        return this.specialItem;
    }
}
