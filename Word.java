
/**
 * Enumeration class Word - write a description of the enum class here
 * 
 * @author (your name here)
 * @version (version number or date here)
 */
public enum Word
{
    HELP("help"),GO("go"),QUIT("quit"),LOOK("look"),EAT("eat"),PICK("pick"),CHECK("check");

    private String commandWord;
    private Word(String commandWord) {
        this.commandWord = commandWord;
    }
    public String getCommandWord() {
        return this.commandWord;
    }
}
