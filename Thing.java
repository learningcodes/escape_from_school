public class Thing
{
     private String name;
     private int energy;
     
     public Thing(String name, int energy) {
         this.name = name;
         this.energy = energy;
     }
     
     public int getEnergy() {return this.energy;}
     
     public String getName() {return this.name;}
}
