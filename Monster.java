
/**
 * 在这里给出对类 Monster 的描述。
 * 
 * @作者（你的名字）
 * @版本（一个版本号或者一个日期）
 */
public class Monster
{
    private String name;
    private int health;
    private int damage;
    
    public Monster(String name, int damage) {
        this.name = name;
        this.damage = damage;
    }
    
    public String getMonsterName() {return this.name;}
    public int getDamage() {return this.damage;}
}
