
/**
 *  This class is the main class of the "World of Zuul" application. 
 *  "World of Zuul" is a very simple, text based adventure game.  Users 
 *  can walk around some scenery. That's all. It should really be extended 
 *  to make it more interesting!
 * 
 *  To play this game, create an instance of this class and call the "play"
 *  method.
 * 
 *  This main class creates and initialises all the others: it creates all
 *  rooms, creates the parser and starts the game.  It also evaluates and
 *  executes the commands that the parser returns.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */

public class Game 
{
    private Parser parser;
    private Room currentRoom;
    private Player player;
    
        
    /**
     * Create the game and initialise its internal map.
     */
    public Game() 
    {
        createRooms();
        parser = new Parser();
        player = new Player();
    }

    /**
     * Create all the rooms and link their exits together.
     */
    private void createRooms()
    {
        //Room outside, theater, pub, lab, office, classroom, parkingLot, toilet, fruitForest;
      
        // create the rooms
        /*outside = new Room("outside the main entrance of the university", new Thing("坚果", 10));
        theater = new Room("in a lecture theater", new Thing("苹果", 30));
        pub = new Room("in the campus pub");
        lab = new Room("in a computing lab");
        office = new Room("in the computing admin office", new Thing("火龙果", 50), new Monster("蝙蝠", 35));
        classroom = new Room("in the school", new Thing("香蕉", 25));
        parkingLot = new Room("in the parking lot", new Monster("老鼠", 15));
        toilet = new Room("in the toilet", new Monster("史莱姆", 10));
        fruitForest = new Room("in the furit forest!", new Thing("桃子", 20));*/
        Room office = new Room("办公室\n你感到背后一阵阴凉",new Monster("教导主任", 10));
        Room toilet = new Room("厕所\n这是一个有味道的地方",new Thing("奥里给",30));
        Room classroom = new Room("教室\n现在是自习课，但你一刻都不想呆在这里");
        Room passage = new Room("走廊\n你想起了你落在寝室里的游戏机");
        Room playground = new Room("操场\n操场上空无一人。西边是校门，也许你可以冲出去");
        Room gate = new Room("校门\n你被门卫大爷捉住了\nGame Over!");
        Room dormitory= new Room("宿舍\n你的游戏机在这里",new Thing("游戏机",-999));
        Room canteen = new Room("食堂\n你在祈祷今天食堂大妈的帕金森没有发作",new Thing("番茄炒西红柿",10));
        Room backdoor = new Room("后门\n看看你发现了什么！从这里可以出去！\n游戏通关！！");
        
        
        office.setExit("south",passage);
        
        toilet.setExit("east",passage);
        
        classroom.setExit("north",passage);
        
        playground.setExit("upstair",passage);
        playground.setExit("west",gate);
        playground.setExit("east",canteen);
        playground.setExit("south",dormitory);
        
        passage.setExit("north",office);
        passage.setExit("west",toilet);
        passage.setExit("south",classroom);
        passage.setExit("downstair",playground);
        
        gate.setExit("east",playground);
        
        dormitory.setExit("north",playground);
        
        canteen.setExit("west",playground);
        canteen.setExit("east",backdoor);
        
        backdoor.setExit("west",canteen);
        
        //currentRoom = passage;
        currentRoom = classroom;
        
        // initialise room exits
        /*
        outside.setExit("east", theater);
        outside.setExit("north", toilet);
        outside.setExit("south", lab);
        //outside.setExit("west", pub);
        outside.setExit("west",cheSuo);

        theater.setExit("west", outside);
        theater.setExit("downstair", parkingLot);
        theater.setExit("north", fruitForest);

        //pub.setExit("east", outside);
        cheSuo.setExit("east",outside);

        lab.setExit("north", outside);
        lab.setExit("east", office);

        office.setExit("west", lab);
        office.setExit("upstair", classroom);

        classroom.setExit("downstair", office);

        parkingLot.setExit("upstair", theater);
        
        toilet.setExit("south", outside);
        toilet.setExit("east", fruitForest);
        
        fruitForest.setExit("west", toilet);
        fruitForest.setExit("south", theater);
        
        currentRoom = outside;  // start game outside
        */
    }

    /**
     *  Main play routine.  Loops until end of play.
     */
    public void play() 
    {            
        printWelcome();

        // Enter the main command loop.  Here we repeatedly read commands and
        // execute them until the game is over.
                
        boolean finished = false;
        while (! finished) {
            Command command = parser.getCommand();
            finished = processCommand(command);
        }
        System.out.println("Thank you for playing.  Good bye.");
    }

    /**
     * Print out the opening message for the player.
     */
    private void printWelcome()
    {
        System.out.println();
        System.out.println("欢迎游玩‘逃离学校’");
        System.out.println("你是一个高中生，但是你厌烦了学校枯燥的学习生活");
        System.out.println("你决定逃离这里");
        System.out.println("输入 'help' 查看指令");
        System.out.println();
        System.out.println("你当前处于" + currentRoom.getDescription());
        currentRoom.printExits();
    }

    /**
     * Given a command, process (that is: execute) the command.
     * @param command The command to be processed.
     * @return true If the command ends the game, false otherwise.
     */
    private boolean processCommand(Command command) 
    {
        boolean wantToQuit = false;
        Thing thing = currentRoom.getThing();

        if(command.isUnknown()) {
            System.out.println("无效的指令");
            return false;
        }
 
        Word commandWord = command.getCommandWord();
        // if (commandWord.equals("help")) {
            // printHelp();
        // }
        // else if (commandWord.equals("go")) {
            // goRoom(command);
        // }
        // else if (commandWord.equals("look")) {
            // say(currentRoom);
        // }
        // else if (commandWord.equals("pick")) {
            // player.addToBag(thing);
            // currentRoom.remove();
        // }
        // else if (commandWord.equals("check")) {
            // player.checkAndPrint();
        // }
        // else if (commandWord.equals("eat")) {
            // player.eatThing(command);
        // }
        // else if (commandWord.equals("quit")) {
            // wantToQuit = quit(command);
        // }
        switch(commandWord) {
            case HELP: 
                printHelp();
                break;
            case GO:
                goRoom(command);
                break;
            case LOOK:
                say(currentRoom);
                break;
            case PICK:
                player.addToBag(thing);
                currentRoom.remove();
                break;
            case CHECK:
                player.checkAndPrint();
                break;
            case EAT:
                player.eatThing(command);
                break;
            case QUIT:
                wantToQuit = quit(command);
        }
        return wantToQuit;
    }
    
    private void say(Room currentRoom) {
        System.out.print("这里");
        if (currentRoom.getThing() == null) {
            System.out.println("什么都没有");
        }
        else System.out.println("有"+currentRoom.getThing().getName());
        if (currentRoom.getMonster() == null) {
            System.out.println("房间安全!");
        }
        else {
            player.hurt(currentRoom.getMonster().getDamage());
            System.out.println("我的天哪! 你被" + currentRoom.getMonster().getMonsterName() +"发现了"+ " !");
            System.out.println("你被训斥了一顿！");
            System.out.println("你损失了" + currentRoom.getMonster().getDamage() + " 点体力!");
            currentRoom.removeMonster();
        }
    }
    // implementations of user commands:

    /**
     * Print out some help information.
     * Here we print some stupid, cryptic message and a list of the 
     * command words.
     */
    private void printHelp() 
    {
        System.out.println("You are lost. You are alone. You wander");
        System.out.println("around at the university.");
        System.out.println();
        System.out.println("Your command words are:");
        System.out.println("   go --前往下一个房间(要在后面追加方向)");
        System.out.println("   help --获取游戏帮助");
        System.out.println("   look -- 检查房间内安全状况");
        System.out.println("   !注意: 如果房间内有怪物, 你将会受到该怪物的攻击, 由于怪物见不得人所以在遇到怪物之后, 怪物会逃跑");
        System.out.println("   pick -- 拾起东西");
        System.out.println("   check -- 查看背包中的物品");
        System.out.println("   eat -- 吃背包中的补给品(在后面追加物品)");
        System.out.println("   quit -- 退出游戏");
    }

    /** 
     * Try to go in one direction. If there is an exit, enter
     * the new room, otherwise print an error message.
     */
    private void goRoom(Command command) 
    {
        if(!command.hasSecondWord()) {
            // if there is no second word, we don't know where to go...
            System.out.println("Go where?");
            return;
        }

        String direction = command.getSecondWord();

        // Try to leave current room.
        Room nextRoom = currentRoom.goNext(direction);
        
        if (nextRoom == null) {
            System.out.println("此路不通");
        }
        else {
            player.step();
            currentRoom = nextRoom;
            System.out.println("你当前处于" + currentRoom.getDescription());
            if(player.getSpecialItem()&&currentRoom.isBackdoor()){
                System.out.println("你成功把游戏机带出来了，声望+1");
            }
            currentRoom.printExits();
        }
    }

    
    /** 
     * "Quit" was entered. Check the rest of the command to see
     * whether we really quit the game.
     * @return true, if this command quits the game, false otherwise.
     */
    private boolean quit(Command command) 
    {
        if(command.hasSecondWord()) {
            System.out.println("Quit what?");
            return false;
        }
        else {
            return true;  // signal that we want to quit
        }
    }
}
