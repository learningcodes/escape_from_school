/**
 * Class Room - a room in an adventure game.
 *
 * This class is part of the "World of Zuul" application. 
 * "World of Zuul" is a very simple, text based adventure game.  
 *
 * A "Room" represents one location in the scenery of the game.  It is 
 * connected to other rooms via exits.  The exits are labelled north, 
 * east, south, west.  For each direction, the room stores a reference
 * to the neighboring room, or null if there is no exit in that direction.
 * 
 * @author  Michael Kölling and David J. Barnes
 * @version 2016.02.29
 */
import java.util.*;

public class Room 
{
    private String description;
    private Map<String, Room> exits = new HashMap<>();
    private Thing thing = null;
    private Monster monster = null;

    /**
     * Create a room described "description". Initially, it has
     * no exits. "description" is something like "a kitchen" or
     * "an open court yard".
     * @param description The room's description.
     */
    public Room(String description) 
    {
        this.description = description;
    }
    
    public Room(String description, Thing thing) {
        this(description);//调用构造函数, 利用已有的构造函数来构造新的构造函数
        this.thing = thing;
    }
    
    public Room(String description, Monster monster) {
        this(description);
        this.monster = monster;
    }
    
    public Room(String description, Thing thing, Monster monster) {
        this(description, thing);
        this.monster = monster;
    }

    public void setExit(String direction, Room room) {
        exits.put(direction, room);
    }

    /**
     * @return The description of the room.
     */
    public String getDescription()
    {
        return description;
    }

    public void printExits() {
        System.out.print("Exits: ");
        for(String key : exits.keySet()) {
            System.out.print(key + " ");
        }
        System.out.println();
    }
    
    public Room goNext(String direction) {
        return exits.get(direction);
    }
    
    public Thing getThing() {
        return this.thing;
    }
    
    public Monster getMonster() {
        return this.monster;
    }
    
    public void remove() {
        this.thing = null;
    }
    
    
    public void removeMonster() {
        this.monster = null;
    }
    
    public boolean isBackdoor(){
        if(this.getDescription() =="后门\n看看你发现了什么！从这里可以出去！\n游戏通关！！")
            return true;
        return false;
    }
}
